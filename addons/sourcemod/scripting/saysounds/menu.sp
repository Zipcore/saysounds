// Adds a Play Admin Sound option to the SourceMod Admin Menu
public void OnAdminMenuReady(Handle topmenu)
{
	/* Block us from being called twice */
	if (topmenu != hAdminMenu)
	{
		hAdminMenu = topmenu;
		TopMenuObject server_commands = FindTopMenuCategory(hAdminMenu, ADMINMENU_SERVERCOMMANDS);
		AddToTopMenu(hAdminMenu, "sm_admin_sounds", TopMenuObject_Item, Play_Admin_Sound,
					 server_commands, "sm_admin_sounds", ADMFLAG_GENERIC);
		AddToTopMenu(hAdminMenu, "sm_karaoke", TopMenuObject_Item, Play_Karaoke_Sound, server_commands, "sm_karaoke", ADMFLAG_CHANGEMAP);
		
		// Added two new items to the admin menu, the soundmenu hide (toggle) and the all sounds menu
		AddToTopMenu(hAdminMenu, "sm_all_sounds", TopMenuObject_Item, Play_All_Sound, server_commands, "sm_all_sounds", ADMFLAG_GENERIC);
		AddToTopMenu(hAdminMenu, "sm_sound_showmenu", TopMenuObject_Item, Set_Sound_Menu, server_commands, "sm_sound_showmenu", ADMFLAG_CHANGEMAP);
	}
}

public void Play_Admin_Sound(Handle topmenu, TopMenuAction action, TopMenuObject object_id, int param, char[] buffer, int maxlength)
{
	if (action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "Play Admin Sound");
	else if (action == TopMenuAction_SelectOption)
		Sound_Menu(param,admin_sounds);
}

public void Play_Karaoke_Sound(Handle topmenu, TopMenuAction action, TopMenuObject object_id, int param, char[] buffer, int maxlength)
{
	if (action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "Karaoke");
	else if (action == TopMenuAction_SelectOption)
		Sound_Menu(param,karaoke_sounds);
}

// This function sets parameters for showing the All Sounds item in the menu
public int Play_All_Sound(Handle topmenu, TopMenuAction action, TopMenuObject object_id, int param, char[] buffer, int maxlength)
{
	if (action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "Play a Sound");
	else if (action == TopMenuAction_SelectOption)
		Sound_Menu(param,all_sounds);
}

// Creates the SoundMenu show/hide item in the admin menu, it is a toggle
public int Set_Sound_Menu(Handle topmenu, TopMenuAction action, TopMenuObject object_id, int param, char[] buffer, int maxlength)
{
	if(GetConVarInt(cvarshowsoundmenu) == 1)
	{
		if (action == TopMenuAction_DisplayOption)
			Format(buffer, maxlength, "Hide Sound Menu");
		else if (action == TopMenuAction_SelectOption)
			SetConVarInt(cvarshowsoundmenu, 0);
	}
	else
	{
		if (action == TopMenuAction_DisplayOption)
			Format(buffer, maxlength, "Show Sound Menu");
		else if (action == TopMenuAction_SelectOption)
			SetConVarInt(cvarshowsoundmenu, 1);
	}
}

void Sound_Menu(int client, sound_types types)
{
	if (types >= admin_sounds)
	{
		AdminId aid = GetUserAdmin(client);
		bool isadmin = (aid != INVALID_ADMIN_ID) && GetAdminFlag(aid, Admin_Generic, Access_Effective);
		if (!isadmin)
		{
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "AdminMenu");
			return;
		}
	}

	Handle soundmenu = CreateMenu(Menu_Select);
	
	SetMenuExitButton(soundmenu, true);
	SetMenuTitle(soundmenu, "Choose a sound to play.");

	char title[PLATFORM_MAX_PATH + 1];
	char buffer[PLATFORM_MAX_PATH + 1];
	char karaokefile[PLATFORM_MAX_PATH + 1];

	KvRewind(listfile);
	if (KvGotoFirstSubKey(listfile))
	{
		do
		{
			KvGetSectionName(listfile, buffer, sizeof(buffer));
			if (!StrEqual(buffer, "JoinSound") && !StrEqual(buffer, "ExitSound") // Skip Join and Exit sounds
				&& !(strlen(buffer) > 15 && StringIsNumeric(buffer))) // Check if the name should be a STEAM64 ID
			{
				if (!KvGetNum(listfile, "actiononly", 0) &&
					KvGetNum(listfile, "enable", 1))
				{
					bool admin = view_as<bool>(KvGetNum(listfile, "admin", 0));
					bool adult = view_as<bool>(KvGetNum(listfile, "adult", 0));
					if (!admin || types >= admin_sounds)
					{
						title[0] = '\0';
						KvGetString(listfile, "title", title, sizeof(title));
						if (!title[0])
							strcopy(title, sizeof(title), buffer);

						karaokefile[0] = '\0';
						KvGetString(listfile, "karaoke", karaokefile, sizeof(karaokefile));
						bool karaoke = (karaokefile[0] != '\0');
						
						// Add type info
						
						if(admin && types != admin_sounds)
							Format(title, sizeof(title), "%s [ADMIN ONLY]", title);
						
						if(adult)
							Format(title, sizeof(title), "%s [Adult ONLY]", title);
						
						if(karaoke && types != karaoke_sounds)
							Format(title, sizeof(title), "%s [Karaoke]", title);
						
						if (!karaoke || types >= karaoke_sounds)
						{
							switch (types)
							{
								case karaoke_sounds:
								{
									if (!karaoke)
										continue;
								}
								case admin_sounds:
								{
									if (!admin)
										continue;
								}
								case all_sounds:
								{
									if (karaoke)
										StrCat(title, sizeof(title), " [Karaoke]");

									if (admin)
										StrCat(title, sizeof(title), " [Admin]");
								}
							}
							
							if(!adult)
								AddMenuItem(soundmenu, buffer, title);
						}
					}
				}
			}
		} while (KvGotoNextKey(listfile));
	}
	else SetFailState("No subkeys found in the config file!");

	DisplayMenu(soundmenu,client,MENU_TIME_FOREVER);
}

public int Menu_Select(Handle menu, MenuAction action, int client, int selection)
{
	if(action==MenuAction_Select)
	{
		char SelectionInfo[PLATFORM_MAX_PATH+1];
		if (GetMenuItem(menu, selection, SelectionInfo, sizeof(SelectionInfo)))
		{
			KvRewind(listfile);
			KvGotoFirstSubKey(listfile);
			char buffer[PLATFORM_MAX_PATH];
			do
			{
				KvGetSectionName(listfile, buffer, sizeof(buffer));
				if (strcmp(SelectionInfo,buffer,false) == 0)
				{
					Submit_Sound(client,buffer);
					break;
				}
			} while (KvGotoNextKey(listfile));
		}
	}
	else if (action == MenuAction_End)
		delete menu;
}

public void SaysoundClientPref(int client, CookieMenuAction action, any info, char[] buffer, int maxlen)
{
	if (action == CookieMenuAction_SelectOption)
	{
		char confMenuFlags[26];
		confMenuFlags[0] = '\0';
		GetConVarString(cvarMenuSettingsFlags, confMenuFlags, sizeof(confMenuFlags));
		
		if (confMenuFlags[0] == '\0' || HasClientFlags(confMenuFlags, client))
			ShowClientPrefMenu(client);
	}
}

public int MenuHandlerClientPref(Handle menu, MenuAction action, int param1, int param2)
{
	if(action == MenuAction_Select)	
	{
		if (param2 == 0)
		{
			// Saysounds
			if(!checkClientCookies(param1, CHK_SAYSOUNDS))
				SetClientCookie(param1, g_sssaysound_cookie, "1");
			else SetClientCookie(param1, g_sssaysound_cookie, "0");
		}
		else if (param2 == 1)
		{
			// Action Sounds
			if(!checkClientCookies(param1, CHK_EVENTS))
				SetClientCookie(param1, g_ssevents_cookie, "1");
			else SetClientCookie(param1, g_ssevents_cookie, "0");
		}
		else if (param2 == 2)
		{
			// Karaoke
			if(!checkClientCookies(param1, CHK_KARAOKE))
				SetClientCookie(param1, g_sskaraoke_cookie, "1");
			else SetClientCookie(param1, g_sskaraoke_cookie, "0");
		}
		else if (param2 == 3)
		{
			// Chat Message
			if(!checkClientCookies(param1, CHK_CHATMSG))
				SetClientCookie(param1, g_sschatmsg_cookie, "1");
			else SetClientCookie(param1, g_sschatmsg_cookie, "0");
		}
		
		ShowClientPrefMenu(param1);
	} 
	else if(action == MenuAction_End)
		delete menu;
}

void ShowClientPrefMenu(int client)
{
	Handle menu = CreateMenu(MenuHandlerClientPref);
	char buffer[100];

	Format(buffer, sizeof(buffer), "%T", "SaysoundsMenu", client);
	SetMenuTitle(menu, buffer);

	// Saysounds
	if(!checkClientCookies(client, CHK_SAYSOUNDS))
		Format(buffer, sizeof(buffer), "%T", "EnableSaysound", client);
	else Format(buffer, sizeof(buffer), "%T", "DisableSaysound", client);

	AddMenuItem(menu, "SaysoundPref", buffer);

	// Action Sounds
	if(!checkClientCookies(client, CHK_EVENTS))
		Format(buffer, sizeof(buffer), "%T", "EnableEvents", client);
	else Format(buffer, sizeof(buffer), "%T", "DisableEvents", client);

	AddMenuItem(menu, "EventPref", buffer);

	// Karaoke
	if(!checkClientCookies(client, CHK_KARAOKE))
		Format(buffer, sizeof(buffer), "%T", "EnableKaraoke", client);
	else Format(buffer, sizeof(buffer), "%T", "DisableKaraoke", client);

	AddMenuItem(menu, "KaraokePref", buffer);

	// Chat Messages
	if(!checkClientCookies(client, CHK_CHATMSG))
		Format(buffer, sizeof(buffer), "%T", "EnableChat", client);
	else Format(buffer, sizeof(buffer), "%T", "DisableChat", client);

	AddMenuItem(menu, "ChatPref", buffer);

	SetMenuExitButton(menu, true);

	DisplayMenu(menu, client, 0);
}