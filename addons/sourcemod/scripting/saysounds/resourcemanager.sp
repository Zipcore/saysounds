//*****************************************************************
//	------------------------------------------------------------- *
//				*** Manage precaching resources ***				  *
//	------------------------------------------------------------- *
//*****************************************************************
#tryinclude "ResourceManager"
#if !defined _ResourceManager_included

#define DONT_DOWNLOAD	0
#define DOWNLOAD		1
#define ALWAYS_DOWNLOAD 2

enum State { 
	Unknown, 
	Defined, 
	Download, 
	Force, 
	Precached 
};

Handle cvarDownloadThreshold;
Handle cvarSoundThreshold;
Handle cvarSoundLimitMap;

int g_iSoundCount			= 0;
int g_iDownloadCount		= 0;
int g_iRequiredCount		= 0;
int g_iPrevDownloadIndex	= 0;
int g_iDownloadThreshold	= -1;
int g_iSoundThreshold		= -1;
int g_iSoundLimit			= -1;

// Trie to hold precache status of sounds
Handle g_soundTrie;

stock bool PrepareSound(const char[] sound, bool force = false, bool preload = false)
{
	State value = Unknown;
	if (!GetTrieValue(g_soundTrie, sound, value) || value < Precached)
	{
		if (force || value >= Force || g_iSoundLimit <= 0 ||
			(g_soundTrie ? GetTrieSize(g_soundTrie) : 0) < g_iSoundLimit)
		{
			PrecacheSound(sound, preload);
			SetTrieValue(g_soundTrie, sound, Precached);
		}
		else
			return false;
	}
	return true;
}

stock void SetupSound(const char[] sound, bool force = false, download = DOWNLOAD, bool precache = false, bool preload = false)
{
	State value = Unknown;
	bool update = !GetTrieValue(g_soundTrie, sound, value);
	if (update || value < Defined)
	{
		g_iSoundCount++;
		value  = Defined;
		update = true;
	}

	if (value < Download && download && g_iDownloadThreshold != 0)
	{
		char file[PLATFORM_MAX_PATH+1];
		Format(file, sizeof(file), "sound/%s", sound);

		if (download < 0) // If download is not specified download it only if it exists in non valve_fs
			download = FileExists(file, false) ? 1 : 0;
		
		if (FileExists(file, false))
		{
			if (download > 0 &&
				(download > 1 || g_iDownloadThreshold < 0 ||
				 (g_iSoundCount > g_iPrevDownloadIndex &&
				  g_iDownloadCount < g_iDownloadThreshold + g_iRequiredCount)))
			{
				AddFileToDownloadsTable(file);

				update = true;
				value  = Download;
				g_iDownloadCount++;

				if (download > 1)
					g_iRequiredCount++;

				if (download <= 1 || g_iSoundCount == g_iPrevDownloadIndex + 1)
					g_iPrevDownloadIndex = g_iSoundCount;
			}
		}
	}

	if (value < Precached && (precache || (g_iSoundThreshold > 0 &&
										   g_iSoundCount < g_iSoundThreshold)))
	{
		if (force || g_iSoundLimit <= 0 &&
			(g_soundTrie ? GetTrieSize(g_soundTrie) : 0) < g_iSoundLimit)
		{
			PrecacheSound(sound, preload);

			if (value < Precached)
			{
				value  = Precached;
				update = true;
			}
		}
	}
	else if (force && value < Force)
	{
		value  = Force;
		update = true;
	}

	if (update)
		SetTrieValue(g_soundTrie, sound, value);
}

stock void PrepareAndEmitSound(const int[] clients,
								int numClients,
								const char[] sample,
								int entity = SOUND_FROM_PLAYER,
								int channel = SNDCHAN_AUTO,
								int level = SNDLEVEL_NORMAL,
								int flags = SND_NOFLAGS,
								float volume = SNDVOL_NORMAL,
								int pitch = SNDPITCH_NORMAL,
								int speakerentity = -1,
								const float origin[3] = NULL_VECTOR,
								const float dir[3] = NULL_VECTOR,
								bool updatePos = true,
								float soundtime = 0.0)
{
	if (PrepareSound(sample))
	{
		if (gb_csgo)
		{
			for (int i = 0; i < numClients; i++)
				ClientCommand(clients[i], "play *%s", sample);
		}
		else
		{
			EmitSound(clients, numClients, sample, entity, channel,
					  level, flags, volume, pitch, speakerentity,
					  origin, dir, updatePos, soundtime);
		}
	}
}

stock void PrepareAndEmitSoundToClient(int client,
										const char[] sample,
										int entity = SOUND_FROM_PLAYER,
										int channel = SNDCHAN_AUTO,
										int level = SNDLEVEL_NORMAL,
										int flags = SND_NOFLAGS,
										float volume = SNDVOL_NORMAL,
										int pitch = SNDPITCH_NORMAL,
										int speakerentity = -1,
										const float origin[3] = NULL_VECTOR,
										const float dir[3] = NULL_VECTOR,
										bool updatePos = true,
										float soundtime = 0.0)
{
	if (PrepareSound(sample))
	{
		if (gb_csgo)
			ClientCommand(client, "play *%s", sample);
		else EmitSoundToClient(client, sample, entity, channel, level, flags, volume, pitch, speakerentity, origin, dir, updatePos, soundtime);
	}
}
#endif
