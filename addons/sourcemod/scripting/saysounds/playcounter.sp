/* Stats - Playcounter*/

void PlayCounter(const char[] soundname)
{
	if(listfile == null)
	{
		LogError("Can't safe playcounter, config not loaded.");
		return;
	}
	
	KvRewind(listfile);
	
	char sName[64], sTitle[64];
	
	if (KvGotoFirstSubKey(listfile))
	{
		do
		{
			KvGetSectionName(listfile, sName, sizeof(sName));
			KvGetString(listfile, "title", sTitle, sizeof(sTitle));
			
			if(StrEqual(soundname, sName) || StrEqual(soundname, sTitle))
			{
				int playcounter = KvGetNum(listfile, "playcount", 0);
				KvSetNum(listfile, "playcount", playcounter + 1);
				KvGoBack(listfile);
				KeyValuesToFile(listfile, soundlistfile);
				return;
			}
			
		} while (KvGotoNextKey(listfile));
	}
	
	LogError("Can't find sound: \"%s\"", soundname);
}

public Action Cmd_TopSounds(int iClient, int iArgs)
{
	if(iClient)
		Menu_TopSounds(iClient);
		
	return Plugin_Handled;
}

void Menu_TopSounds(int iClient)
{
	ArrayList aPlayCount = new ArrayList();
	
	KvRewind(listfile);
	int iIndex = 0;
	if (KvGotoFirstSubKey(listfile))
	{
		do
		{
			if(KvGetNum(listfile, "playcount", 0)) // If the sound wasn't counted we don't need to list it, this will skip event sounds too
				aPlayCount.Push(iIndex);
			iIndex++;
			
		} while (KvGotoNextKey(listfile));
	}
	
	if(aPlayCount.Length < 1)
	{
		PrintToChat(iClient,"\x04[Say Sounds]\x01 Not enough data yet, sorry.");
		return;
	}
	
	SortADTArrayCustom(aPlayCount, SortTopSounds);
	
	Menu menu = new Menu(Menu_Topsounds_Callback);
	
	menu.SetTitle("Top Sounds");
	menu.ExitBackButton = true;
	
	char sBuffer[128];
	int iCount;
	for (int i = 0; i < aPlayCount.Length; i++)
	{
		KV_JumpTo(listfile, aPlayCount.Get(i));
		iCount = KvGetNum(listfile, "playcount", 0);
		
		KvGetString(listfile, "title", sBuffer, sizeof(sBuffer));
		if(StrEqual(sBuffer, "")) // Use section name if no title is set
			KvGetSectionName(listfile, sBuffer, sizeof(sBuffer));
			
		Format(sBuffer, sizeof(sBuffer), "#%i - %s [%i]", i+1, sBuffer, iCount);
		menu.AddItem("", sBuffer);
	}
	
	menu.Display(iClient, MENU_TIME_FOREVER);
	
	delete aPlayCount;
}

public int Menu_Topsounds_Callback(Menu menu, MenuAction action, int iClient, int iInfo)
{
	if (action == MenuAction_Select)
	{
		//
	}
	else if(action == MenuAction_End)
		delete menu;
}

stock bool KV_JumpTo(Handle kv, int index)
{
	KvRewind(kv);
	
	if (!KvGotoFirstSubKey(kv, false))
		return false;
	
	for (int i = 0; i < index; i++)
	{
		if (!KvGotoNextKey(kv, false))
			return false;
	}
	
	return true;
}

public int SortTopSounds(int index1, int index2, Handle array, Handle hndl)
{
	int iCount1, iCount2;
	KV_JumpTo(listfile, GetArrayCell(array, index1));
	iCount1 = KvGetNum(listfile, "playcount", 0);
	
	KV_JumpTo(listfile, GetArrayCell(array, index2));
	iCount2 = KvGetNum(listfile, "playcount", 0);
	
	int sort;
	
	if(iCount1 > iCount2)
		sort = -1;
	else if(iCount1 < iCount2)
		sort = 1;
	
	return sort;
}
