/*
 * vim: set ai et ts=4 sw=4 :

Cvarlist (default value):
	sm_sound_enable					1	  Turns Sounds On/Off
	sm_sound_warn					3	  Number of sounds to warn person at
	sm_sound_limit				   	5	  Maximum sounds per person
	sm_sound_admin_limit			0	  Maximum sounds per admin
	sm_sound_admin_warn				0		Number of sounds to warn admin at
	sm_sound_announce				0	  Turns on announcements when a sound is played
	sm_sound_sentence				0	  When set, will trigger sounds if keyword is embedded in a sentence
	sm_sound_logging				0	  When set, will log sounds that are played
	sm_join_exit					0	  Play sounds when someone joins or exits the game
	sm_join_spawn					1	  Wait until the player spawns before playing the join sound
	sm_specific_join_exit			0	  Play sounds when a specific STEAM ID joins or exits the game
	sm_time_between_sounds		   	4.5		Time between each sound trigger, 0.0 to disable checking
	sm_time_between_admin_sounds	4.5		Time between each sound trigger (for admins), 0.0 to disable checking
	sm_sound_showmenu				1	  Turns the Public Sound Menu on(1) or off(0)
	sm_saysounds_volume			  	1.0		Global/Default Volume setting for Say Sounds (0.0 <= x <= 1.0).
	sm_saysound_interrupt_sound	1	  If set, interrupt the current sound when a new start
	sm_saysound_filter_if_dead	 0	  If set, alive players do not hear sounds triggered by dead players
	sm_saysound_download_threshold -1	 If set, sets the number of sounds that are downloaded per map.
	sm_saysound_sound_threshold	0	  If set, sets the number of sounds that are precached on map start (in SetupSound).
	sm_saysound_sound_max		  -1	 If set, set max number of sounds that can be used on a map.
	sm_saysound_track_disconnects  1	  If set, stores sound counts when clients leave and loads them when they join.
	
Admin Commands:
	sm_sound_ban <user>		 Bans a player from using sounds
	sm_sound_unban <user>		 Unbans a player os they can play sounds
	sm_sound_reset <all|user>	 Resets sound quota for user, or everyone if all
	sm_admin_sounds 		 Display a menu of all admin sounds to play
	!adminsounds 			 When used in chat will present a menu of all admin sound to play.
	!allsounds			 When used in chat will present a menu of all sounds to play.
	
User Commands:
	sm_sound_menu 			 Display a menu of all sounds (trigger words) to play
	sm_sound_list  			 Print all trigger words to the console
	!sounds  			 When used in chat turns sounds on/off for that client
	!soundlist  			 When used in chat will print all the trigger words to the console (Now displays menu)
	!soundmenu  			 When used in chat will present a menu to choose a sound to play.
	!stop	 			 When used in chat will per-user stop any sound currently playing by this plug-in

*/

#include <sourcemod>
#include <sdktools>
#include <clientprefs>
#undef REQUIRE_EXTENSIONS
#include <tf2_stocks>
#define REQUIRE_EXTENSIONS

#include <soundlib>

#undef REQUIRE_PLUGIN
#include <adminmenu>

// extra memory usability for a lot of sounds.
//#pragma dynamic 65536 
#pragma dynamic 13107

#pragma semicolon 1

#define PLUGIN_VERSION "4.0.8"

#define CHK_CHATMSG		1
#define CHK_SAYSOUNDS	2
#define CHK_EVENTS		3
#define CHK_KARAOKE		4
#define CHK_BANNED		5
#define CHK_GREETED		6

#define TF2_ATTENTION	"vo/announcer_attention.wav"
#define TF2_20_SECONDS	"vo/announcer_begins_20sec.wav"
#define TF2_10_SECONDS	"vo/announcer_begins_10sec.wav"
#define TF2_5_SECONDS	"vo/announcer_begins_5sec.wav"
#define TF2_4_SECONDS	"vo/announcer_begins_4sec.wav"
#define TF2_3_SECONDS	"vo/announcer_begins_3sec.wav"
#define TF2_2_SECONDS	"vo/announcer_begins_2sec.wav"
#define TF2_1_SECOND	"vo/announcer_begins_1sec.wav"

#define HL2_ATTENTION	"npc/overwatch/radiovoice/attention.wav"
#define HL2_10_SECONDS "npc/overwatch/cityvoice/fcitadel_10sectosingularity.wav"
#define HL2_5_SECONDS	"npc/overwatch/radiovoice/five.wav"
#define HL2_4_SECONDS	"npc/overwatch/radiovoice/four.wav"
#define HL2_3_SECONDS	"npc/overwatch/radiovoice/three.wav"
#define HL2_2_SECONDS	"npc/overwatch/radiovoice/two.wav"
#define HL2_1_SECOND	"npc/overwatch/radiovoice/one.wav"

enum sound_types { 
	normal_sounds, 
	admin_sounds, 
	karaoke_sounds, 
	all_sounds 
};

ConVar cvarsoundenable;
ConVar cvarsoundlimit;
ConVar cvarsoundlimitFlags;
ConVar cvarsoundFlags;
ConVar cvarsoundFlagsLimit;
ConVar cvarsoundwarn;
ConVar cvarjoinexit;
ConVar cvarjoinspawn;
ConVar cvarspecificjoinexit;
ConVar cvartimebetween;
ConVar cvartimebetweenFlags;
ConVar cvaradmintime;
ConVar cvaradminwarn;
ConVar cvaradminlimit;
ConVar cvarannounce;
ConVar cvaradult;
ConVar cvarsentence;
ConVar cvarlogging;
ConVar cvarplayifclsndoff;
ConVar cvarkaraokedelay;
ConVar cvarvolume;
ConVar cvarsoundlimitround;
ConVar cvarexcludelastsound;
ConVar cvarblocktrigger;
ConVar cvarinterruptsound;
ConVar cvarfilterifdead;
ConVar cvarTrackDisconnects;
ConVar cvarStopFlags;
ConVar cvarMenuSettingsFlags;
Handle g_hMapvoteDuration;
ConVar cvarshowsoundmenu;
Handle listfile;
Handle hAdminMenu;
Handle g_hSoundCountTrie;
char soundlistfile[PLATFORM_MAX_PATH] = "";

Handle g_sssaysound_cookie;	// Cookie for storing clints saysound setting (ON/OFF)
Handle g_ssevents_cookie;	// Cookie for storing clients eventsound setting (ON/OFF)
Handle g_sschatmsg_cookie;	// Cookie for storing clients chat message setting (ON/OFF)
Handle g_sskaraoke_cookie; // Cookie for storing clients karaoke setting (ON/OFF)
Handle g_ssban_cookie;		// Cookie for storing if client is banned from using saysiunds
Handle g_ssgreeted_cookie;	// Cookie for storing if we've played the welcome sound to the client

int SndCount[MAXPLAYERS+1];
char SndPlaying[MAXPLAYERS+1][PLATFORM_MAX_PATH];
bool firstSpawn[MAXPLAYERS+1];
float globalLastSound = 0.0;
float globalLastAdminSound = 0.0;
char LastPlayedSound[PLATFORM_MAX_PATH+1] = "";
bool hearalive = true;
bool gb_csgo = false;

Handle karaokeFile = null;
Handle karaokeTimer = null;
float karaokeStartTime = 0.0;

Handle cvaradvertisements = null;
bool advertisements_enabled = false;

//	Kill Event: If someone kills a few clients with a crit, make sure he won't get spammed with the corresponding sound
bool g_bPlayedEvent2Client[MAXPLAYERS+1] = false;

public Plugin myinfo = 
{
	name = "Say Sounds (including Hybrid Edition)",
	author = "Hell Phoenix|Naris|FernFerret|Uberman|psychonic|edgecom|woody|Miraculix|gH0sTy|Zipcore",
	description = "Say Sounds and Action Sounds packaged into one neat plugin! Welcome to the new day of SaySounds Hybrid!",
	version = PLUGIN_VERSION,
	url = "http://forums.alliedmods.net/showthread.php?t=82220"
};

#include "saysounds/gametype.sp"
#include "saysounds/resourcemanager.sp"
#include "saysounds/checks.sp"
#include "saysounds/menu.sp"
#include "saysounds/playcounter.sp"

public void OnPluginStart()
{
	if(GetGameType() == l4d2 || GetGameType() == l4d)
		SetFailState("The Left 4 Dead series is not supported!");
	
	LoadTranslations("common.phrases");
	LoadTranslations("saysoundhe.phrases");
	
	/* ConVars */
	
	cvarsoundenable = CreateConVar("sm_saysound_enable","1","Turns Sounds On/Off");
	
	cvarsoundwarn = CreateConVar("sm_saysound_sound_warn","3","Number of sounds to warn person at (0 for no warnings)");
	cvarsoundlimit = CreateConVar("sm_saysound_sound_limit","5","Maximum sounds per person (0 for unlimited)");
	cvarsoundlimitFlags = CreateConVar("sm_saysound_sound_flags","","User flags that will result in unlimited sounds");
	cvarsoundFlags = CreateConVar("sm_saysound_flags","","Flag(s) that will have a seperate sound limit");
	cvarsoundFlagsLimit = CreateConVar("sm_saysound_flags_limit","5","Maximum sounds per person with the corresponding flag (0 for unlimited)");
	
	cvarjoinexit = CreateConVar("sm_saysound_join_exit","0","Play sounds when someone joins or exits the game");
	cvarjoinspawn = CreateConVar("sm_saysound_join_spawn","1","Wait until the player spawns before playing the join sound");
	cvarspecificjoinexit = CreateConVar("sm_saysound_specific_join_exit","1","Play sounds when specific steam ID joins or exits the game");
	
	cvartimebetween = CreateConVar("sm_saysound_time_between_sounds","4.5","Time between each sound trigger, 0.0 to disable checking");
	cvartimebetweenFlags = CreateConVar("sm_saysound_time_between_flags","","User flags to bypass the Time between sounds check");
	
	cvaradmintime = CreateConVar("sm_saysound_time_between_admin_sounds","4.5","Time between each admin sound trigger, 0.0 to disable checking for admin sounds \nSet to -1 to completely bypass the soundspam protection for admins");
	cvaradminwarn = CreateConVar("sm_saysound_sound_admin_warn","0","Number of sounds to warn admin at (0 for no warnings)");
	cvaradminlimit = CreateConVar("sm_saysound_sound_admin_limit","0","Maximum sounds per admin (0 for unlimited)");
	cvarsoundlimitround = CreateConVar("sm_saysound_limit_sound_per_round", "0", "If set, sm_saysound_sound_limit is the limit per round instead of per map");
	cvarannounce = CreateConVar("sm_saysound_sound_announce","1","Turns on announcements when a sound is played");
	cvaradult = CreateConVar("sm_saysound_adult_announce","0","Announce played adult sounds? | 0 = off 1 = on");
	cvarsentence = CreateConVar("sm_saysound_sound_sentence","0","When set, will trigger sounds if keyword is embedded in a sentence");
	cvarlogging = CreateConVar("sm_saysound_sound_logging","0","When set, will log sounds that are played");
	cvarvolume = CreateConVar("sm_saysound_saysounds_volume", "1.0", "Volume setting for Say Sounds (0.0 <= x <= 1.0)", _, true, 0.0, true, 1.0);
	cvarplayifclsndoff = CreateConVar("sm_saysound_play_cl_snd_off","0","When set, allows clients that have turned their sounds off to trigger sounds (0=off | 1=on)");
	cvarkaraokedelay = CreateConVar("sm_saysound_karaoke_delay","15.0","Delay before playing a Karaoke song");
	cvarexcludelastsound = CreateConVar("sm_saysound_excl_last_sound", "0", "If set, don't allow to play a sound that was recently played");
	cvarblocktrigger = CreateConVar("sm_saysound_block_trigger", "0", "If set, block the sound trigger to be displayed in the chat window");
	cvarinterruptsound = CreateConVar("sm_saysound_interrupt_sound", "0", "If set, interrupt the current sound when a new start");
	cvarfilterifdead = CreateConVar("sm_saysound_filter_if_dead", "0", "If set, alive players do not hear sounds triggered by dead players");
	cvarTrackDisconnects = CreateConVar("sm_saysound_track_disconnects", "1", "If set, stores sound counts when clients leave and loads them when they join.");
	cvarStopFlags = CreateConVar("sm_saysound_stop_flags","","User flags that are allowed to stop a sound");
	cvarMenuSettingsFlags = CreateConVar("sm_saysound_confmenu_flags","","User flags that are allowed to access the configuration menu");

#if !defined _ResourceManager_included
	cvarDownloadThreshold = CreateConVar("sm_saysound_download_threshold", "-1", "Number of sounds to download per map start (-1=unlimited).");
	cvarSoundThreshold = CreateConVar("sm_saysound_sound_threshold", "0", "Number of sounds to precache on map start (-1=unlimited).");
	cvarSoundLimitMap	 = CreateConVar("sm_saysound_sound_max", "-1", "Maximum number of sounds to allow (-1=unlimited).");
#endif
	
	// This is the Variable that will enable or disable the sound menu to public users, Admin users will always have
	// access to their menus, From the admin menu it is a toggle variable
	cvarshowsoundmenu = CreateConVar("sm_saysound_showmenu","1","1 To show menu to users, 0 to hide menu from users (admins excluded)");
	
	g_sssaysound_cookie = RegClientCookie("saysoundsplay", "Play Say Sounds", CookieAccess_Protected);
	g_ssevents_cookie = RegClientCookie("saysoundsevent", "Play Action sounds", CookieAccess_Protected);
	g_sschatmsg_cookie = RegClientCookie("saysoundschatmsg", "Display Chat messages", CookieAccess_Protected);
	g_sskaraoke_cookie = RegClientCookie("saysoundskaraoke", "Play Karaoke music", CookieAccess_Protected);
	g_ssban_cookie = RegClientCookie("saysoundsban", "Banned From Say Sounds", CookieAccess_Protected);
	g_ssgreeted_cookie = RegClientCookie("saysoundsgreeted", "Join sound Cache", CookieAccess_Protected);
	
	SetCookieMenuItem(SaysoundClientPref, 0, "Say Sounds Preferences");
	
	HookConVarChange(cvarsoundenable, EnableChanged);

	RegAdminCmd("sm_sound_ban", Command_Sound_Ban, ADMFLAG_BAN, "sm_sound_ban <user> : Bans a player from using sounds");
	RegAdminCmd("sm_sound_unban", Command_Sound_Unban, ADMFLAG_BAN, "sm_sound_unban <user> : Unbans a player from using sounds");
	RegAdminCmd("sm_sound_reset", Command_Sound_Reset, ADMFLAG_GENERIC, "sm_sound_reset <user | all> : Resets sound quota for user, or everyone if all");
	RegAdminCmd("sm_admin_sounds", Command_Admin_Sounds,ADMFLAG_GENERIC, "Display a menu of Admin sounds to play");
	RegAdminCmd("sm_karaoke", Command_Karaoke, ADMFLAG_GENERIC, "Display a menu of Karaoke songs to play");
	RegAdminCmd("sm_all_sounds", Command_All_Sounds, ADMFLAG_GENERIC,"Display a menu of ALL sounds to play");

	RegConsoleCmd("sm_sound_list", Command_Sound_List, "List available sounds to console");
	RegConsoleCmd("sm_sound_menu", Command_Sound_Menu, "Display a menu of sounds to play");
	RegConsoleCmd("sm_topsounds", Cmd_TopSounds, "Displays most played sounds as a menu");
	
	AddCommandListener(Command_Say, "say");
	AddCommandListener(Command_Say, "say2");
	AddCommandListener(Command_Say, "say_team");
	
	AutoExecConfig(true, "sm_saysounds");
	
	if(GetConVarBool(cvarsoundenable)) 
	{
		HookEvent("player_death", Event_Kill);
		HookEvent("player_disconnect", Event_Disconnect, EventHookMode_Pre);
		HookEventEx("player_spawn",PlayerSpawn);

		if (GetGameType() == tf2)
		{
			LogMessage("[Say Sounds] Detected Team Fortress 2");
			HookEvent("teamplay_flag_event", Event_Flag);
			HookEvent("player_chargedeployed", Event_UberCharge);
			HookEvent("player_builtobject", Event_Build);
			HookEvent("teamplay_round_start", Event_RoundStart);
			HookEvent("teamplay_setup_finished", Event_SetupEnd);
			HookEvent("teamplay_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == dod)
		{
			LogMessage("[Say Sounds] Detected Day of Defeat");
			HookEvent("player_hurt", Event_Hurt);
			HookEvent("dod_round_start", Event_RoundStart);
			HookEvent("dod_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == zps)
		{
			LogMessage("[Say Sounds] Detected Zombie Panic:Source");
			HookEvent("game_round_restart", Event_RoundStart);
		}
		else if (GetGameType() == cstrike)
		{
			LogMessage("[Say Sounds] Detected Counter Strike");
			HookEvent("round_start", Event_RoundStart);
			HookEvent("round_end", Event_RoundEnd);
		}
		else if (GetGameType() == hl2mp)
		{
			LogMessage("[Say Sounds] Detected Half-Life 2 Deathmatch");
			HookEvent("teamplay_round_start",Event_RoundStart);
		}
		else if (GetGameType() == csgo)
		{
			LogMessage("[Say Sounds] Detected Counter-Strike: Global Offensive");
			HookEvent("round_start", Event_RoundStart);
			gb_csgo = true;
		}
		else if (GetGameType() == other_game)
		{
			LogMessage("[Say Sounds] No specific game detected");
			HookEvent("round_start", Event_RoundStart);
		}
		
		if (GetConVarBool(cvarTrackDisconnects) &&
			(GetConVarInt(cvarsoundlimit) > 0 ||
			 GetConVarInt(cvaradminlimit) > 0 ||
			 GetConVarInt(cvarsoundFlagsLimit) > 0))
		{
			g_hSoundCountTrie = CreateTrie();
		}
	}
	
	Handle topmenu;
	if (LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != null))
		OnAdminMenuReady(topmenu);
}

public void EnableChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	int intNewValue = StringToInt(newValue);
	int intOldValue = StringToInt(oldValue);

	if(intNewValue == 1 && intOldValue == 0) 
	{
		LogMessage("[Say Sounds] Enabled, Hooking Events");
		
		AddCommandListener(Command_Say, "say");
		AddCommandListener(Command_Say, "say2");
		AddCommandListener(Command_Say, "say_team");

		HookEvent("player_death", Event_Kill);
		HookEvent("player_disconnect", Event_Disconnect, EventHookMode_Pre);
		HookEventEx("player_spawn", PlayerSpawn);

		if (GetGameType() == tf2)
		{
			LogMessage("[Say Sounds] Detected Team Fortress 2");
			HookEvent("teamplay_flag_event", Event_Flag);
			HookEvent("player_chargedeployed", Event_UberCharge);
			HookEvent("player_builtobject", Event_Build);
			HookEvent("teamplay_round_start", Event_RoundStart);
			HookEvent("teamplay_setup_finished", Event_SetupEnd);
			HookEvent("teamplay_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == dod)
		{
			LogMessage("[Say Sounds] Detected Day of Defeat");
			HookEvent("player_hurt", Event_Hurt);
			HookEvent("dod_round_start", Event_RoundStart);
			HookEvent("dod_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == zps)
		{
			LogMessage("[Say Sounds] Detected Zombie Panic:Source");
			HookEvent("game_round_restart", Event_RoundStart);
		}
		else if (GetGameType() == cstrike)
		{
			LogMessage("[Say Sounds] Detected Counter Strike");
			HookEvent("round_start", Event_RoundStart);
			HookEvent("round_end", Event_RoundEnd);
		}
		else if (GetGameType() == hl2mp)
		{
			LogMessage("[Say Sounds] Detected Half-Life 2 Deathmatch");
			HookEvent("teamplay_round_start",Event_RoundStart);
		}
		else if (GetGameType() == csgo)
		{
			LogMessage("[Say Sounds] Detected Counter-Strike: Global Offensive");
			HookEvent("round_start", Event_RoundStart);
			gb_csgo = true;
		}
		else if (GetGameType() == other_game)
		{
			LogMessage("[Say Sounds] No specific game detected");
			HookEvent("round_start", Event_RoundStart);
		}

		ClearSoundCountTrie();
	}
	else if(intNewValue == 0 && intOldValue == 1) 
	{
		LogMessage("[Say Sounds] Disabled, Unhooking Events");
		
		RemoveCommandListener(Command_Say, "say");
		RemoveCommandListener(Command_Say, "say2");
		RemoveCommandListener(Command_Say, "say_team");

		UnhookEvent("player_death", Event_Kill);
		UnhookEvent("player_disconnect", Event_Disconnect, EventHookMode_Pre);
		UnhookEvent("player_spawn",PlayerSpawn);

		if (GetGameType() == tf2)
		{
			UnhookEvent("teamplay_flag_event", Event_Flag);
			UnhookEvent("player_chargedeployed", Event_UberCharge);
			UnhookEvent("player_builtobject", Event_Build);
			UnhookEvent("teamplay_round_start", Event_RoundStart);
			UnhookEvent("teamplay_setup_finished", Event_SetupEnd);
			UnhookEvent("teamplay_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == dod)
		{
			UnhookEvent("player_hurt", Event_Hurt);
			UnhookEvent("dod_round_start", Event_RoundStart);
			UnhookEvent("dod_broadcast_audio", OnAudioBroadcast, EventHookMode_Pre);
		}
		else if (GetGameType() == zps){

			UnhookEvent("game_round_restart", Event_RoundStart);
		}
		else if (GetGameType() == cstrike)
		{
			UnhookEvent("round_start", Event_RoundStart);
			UnhookEvent("round_end", Event_RoundEnd);
		}
		else if (GetGameType() == hl2mp)
			UnhookEvent("teamplay_round_start",Event_RoundStart);
		else if (GetGameType() == csgo)
			UnhookEvent("round_start", Event_RoundStart);
		else if (GetGameType() == other_game)
			UnhookEvent("round_start", Event_RoundStart);
	}
}

public OnPluginEnd()
{
	if (listfile != null)
	{
		delete listfile;
		listfile = null;
	}

	if (karaokeFile != null)
	{
		delete karaokeFile;
		karaokeFile = null;
	}
}

public void OnMapStart()
{
	LastPlayedSound = "";
	globalLastSound = 0.0;
	globalLastAdminSound = 0.0;

	for (int i = 1; i <= MAXPLAYERS; i++)
	{
		SndCount[i] = 0;
	}

	ClearSoundCountTrie();

	#if !defined _ResourceManager_included
		g_iDownloadThreshold = GetConVarInt(cvarDownloadThreshold);
		g_iSoundThreshold	= GetConVarInt(cvarSoundThreshold);
		g_iSoundLimit		= GetConVarInt(cvarSoundLimitMap);

		// Setup trie to keep track of precached sounds
		if (g_soundTrie == null)
			g_soundTrie = CreateTrie();
		else ClearTrie(g_soundTrie);
	#endif

	CreateTimer(0.1, Load_Sounds);
}

public void OnMapEnd()
{
	if (g_hSoundCountTrie != null)
	{
		delete g_hSoundCountTrie;
		g_hSoundCountTrie = null;
	}

	if (listfile != null)
	{
		delete listfile;
		listfile = null;
	}

	if (karaokeFile != null)
	{
		delete karaokeFile;
		karaokeFile = null;
	}

	if (karaokeTimer != null)
	{
		KillTimer(karaokeTimer);
		karaokeTimer = null;
	}

#if !defined _ResourceManager_included
	if (g_iPrevDownloadIndex >= g_iSoundCount ||
		g_iDownloadCount < g_iDownloadThreshold)
	{
		g_iPrevDownloadIndex = 0;
	}

	g_iDownloadCount	 = 0;
	g_iSoundCount		= 0;
#endif
}

public void OnMapVoteStarted()
{
	g_hMapvoteDuration = FindConVar("sm_mapvote_voteduration");
	
	if (g_hMapvoteDuration != null)
		CreateTimer(GetConVarFloat(g_hMapvoteDuration), TimerMapvoteEnd);
	else LogError("ConVar sm_mapvote_voteduration not found!");
	
	runSoundEvent(null,"mapvote","start",0,0,-1);
}

public Action TimerMapvoteEnd(Handle timer, any data)
{
	runSoundEvent(null, "mapvote", "end", 0, 0, -1);
}

public TF2_OnWaitingForPlayersStart()
{
	runSoundEvent(null,"wait4players","start",0,0,-1);
}

public TF2_OnWaitingForPlayersEnd()
{
	runSoundEvent(null,"wait4players","end",0,0,-1);
}

public Action Load_Sounds(Handle timer, any data)
{
	// precache sounds, loop through sounds
	BuildPath(Path_SM,soundlistfile,sizeof(soundlistfile),"configs/saysounds.cfg");
	if(!FileExists(soundlistfile))
		SetFailState("saysounds.cfg not parsed...file doesnt exist!");
	else
	{
		if (listfile != null)
			delete listfile;

		listfile = CreateKeyValues("soundlist");
		FileToKeyValues(listfile, soundlistfile);
		KvRewind(listfile);
		if (KvGotoFirstSubKey(listfile))
		{
			do
			{
				if (KvGetNum(listfile, "enable", 1))
				{
					char filelocation[PLATFORM_MAX_PATH+1];
					char file[8];
					int count = KvGetNum(listfile, "count", 1);
					int download = KvGetNum(listfile, "download", 1);
					bool force = view_as<bool>(KvGetNum(listfile, "force", 0));
					bool precache = view_as<bool>(KvGetNum(listfile, "precache", 0));
					bool preload = view_as<bool>(KvGetNum(listfile, "preload", 0));
					for (int i = 0; i <= count; i++)
					{
						if (i)
							Format(file, sizeof(file), "file%d", i);
						else strcopy(file, sizeof(file), "file");

						filelocation[0] = '\0';
						KvGetString(listfile, file, filelocation, sizeof(filelocation), "");
						if (filelocation[0] != '\0')
							SetupSound(filelocation, force, download, precache, preload);
					}
				}
			} while (KvGotoNextKey(listfile));
		}
		else SetFailState("saysounds.cfg not parsed...No subkeys found!");
	}
	return Plugin_Handled;
}

void ResetClientSoundCount()
{
	for (int i = 1; i <= MAXPLAYERS; i++)
		SndCount[i] = 0;

	ClearSoundCountTrie();
}

public Action reset_PlayedEvent2Client(Handle timer, any client)
{
	g_bPlayedEvent2Client[client] = false;
}

public Action Event_Disconnect(Handle event, const char[] name, bool dontBroadcast)
{
	char SteamID[60];
	GetEventString(event, "networkid", SteamID, sizeof(SteamID));
	SetAuthIdCookie(SteamID, g_ssgreeted_cookie, "0");
	int id2Client = GetClientOfUserId(GetEventInt(event, "userid"));

	if (g_hSoundCountTrie != null)
		SetTrieValue(g_hSoundCountTrie, SteamID, SndCount[id2Client]);
}

public Action OnAudioBroadcast(Handle event, const char[] name, bool dontBroadcast)
{
	if(!GetConVarBool(cvarsoundenable))
		return Plugin_Continue;

	char sound[30];
	GetEventString(event, "sound", sound, sizeof(sound));

	if(GetGameType() == tf2)
	{
		int iTeam = GetEventInt(event, "team");
		if (StrEqual(sound, "Game.Stalemate") && runSoundEvent(event, "round", "Stalemate", 0, 0, -1))
			return Plugin_Handled;
		else if (StrEqual(sound, "Game.SuddenDeath") && runSoundEvent(event, "round", "SuddenDeath", 0, 0, -1))
			return Plugin_Handled;
		else if (StrEqual(sound, "Game.YourTeamWon") && runSoundEvent(event, "round", "won", 0, 0, iTeam))
			return Plugin_Handled;
		else if (StrEqual(sound, "Game.YourTeamLost") && runSoundEvent(event, "round", "lost", 0, 0, iTeam))
			return Plugin_Handled;
	}
	else if(GetGameType() == dod)
	{
		if (StrEqual(sound, "Game.USWin") && runSoundEvent(event, "round", "USWon", 0, 0, -1))
			return Plugin_Handled;
		else if (StrEqual(sound, "Game.GermanWin") && runSoundEvent(event, "round", "GERWon", 0, 0, -1))
			return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action Event_SetupEnd(Handle event, const char[] name, bool dontBroadcast)
{
	runSoundEvent(event, "round", "setupend", 0, 0, -1);
	return Plugin_Continue;
}

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	runSoundEvent(event, "round", "start", 0, 0, -1);

	if (GetConVarBool(cvarsoundlimitround))
		ResetClientSoundCount();

	return Plugin_Continue;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	runSoundEvent(event, "round", "end", 0, 0, -1);
	return Plugin_Continue;
}

public Action Event_UberCharge(Handle event, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(GetEventInt(event, "userid"));
	int victim   = GetClientOfUserId(GetEventInt(event, "targetid"));
	runSoundEvent(event,"uber","uber",attacker,victim,-1);
	return Plugin_Continue;
}

public Action Event_Flag(Handle event, const char[] name, bool dontBroadcast)
{
	// pick up(1), capture(2), defend(3), dropped(4)
	// Translate the Integer that is the input to a string so that users
	// can just add a string to the config file
	char flagstring[PLATFORM_MAX_PATH+1];
	int flagint;
	flagint = GetEventInt(event, "eventtype");
	switch(flagint)
	{
		case 1:
			strcopy(flagstring,sizeof(flagstring),"flag_picked_up");
		case 2:
			strcopy(flagstring,sizeof(flagstring),"flag_captured");
		case 3:
			strcopy(flagstring,sizeof(flagstring),"flag_defended");
		case 4:
			strcopy(flagstring,sizeof(flagstring),"flag_dropped");
		default:
			strcopy(flagstring,sizeof(flagstring),"flag_captured");
	}
	runSoundEvent(event, "flag", flagstring, 0, 0, -1);
	return Plugin_Continue;
}

public Action Event_Kill(Handle event, const char[] name, bool dontBroadcast)
{
	char wepstring[PLATFORM_MAX_PATH+1];
	
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int victim   = GetClientOfUserId(GetEventInt(event, "userid"));

	if (attacker == victim)
	{
		runSoundEvent(event,"kill","suicide",0,victim,-1);
		return Plugin_Continue;
	}
	else
	{
		GetEventString(event, "weapon_logclassname",wepstring,PLATFORM_MAX_PATH+1);

		if (GetGameType() == tf2)
		{
			int custom_kill = GetEventInt(event, "customkill");
			if (custom_kill == TF_CUSTOM_HEADSHOT)
			{
				runSoundEvent(event,"kill","headshot",attacker,victim,-1);
				return Plugin_Continue;
			}
			if (custom_kill == TF_CUSTOM_BACKSTAB)
			{
				runSoundEvent(event,"kill","backstab",attacker,victim,-1);
				return Plugin_Continue;
			}
			if (custom_kill == TF_CUSTOM_TELEFRAG)
			{
				runSoundEvent(event,"kill","telefrag",attacker,victim,-1);
				return Plugin_Continue;
			}
			int bits = GetEventInt(event,"damagebits");
			if (bits & 1048576 && attacker > 0)
			{
				runSoundEvent(event,"kill","crit_kill",attacker,victim,-1);
				return Plugin_Continue;
			}
			if (bits == 16 && victim > 0)
			{
				runSoundEvent(event,"kill","hit_by_train",0,victim,-1);
				return Plugin_Continue;
			}
			if (bits == 16384 && victim > 0)
			{
				runSoundEvent(event,"kill","drowned",0,victim,-1);
				return Plugin_Continue;
			}
			if (bits & 32 && victim > 0)
			{
				runSoundEvent(event,"kill","fall",0,victim,-1);
				return Plugin_Continue;
			}

			GetEventString(event, "weapon_logclassname",wepstring,PLATFORM_MAX_PATH+1);
		}
		else if (GetGameType() == cstrike || GetGameType() == csgo)
		{
			int headshot = 0;
			headshot = GetEventBool(event, "headshot");
			if (headshot == 1)
			{
				runSoundEvent(event,"kill","headshot",attacker,victim,-1);
				return Plugin_Continue;
			}
			GetEventString(event, "weapon",wepstring,PLATFORM_MAX_PATH+1);
		}
		else GetEventString(event, "weapon",wepstring,PLATFORM_MAX_PATH+1);

		runSoundEvent(event,"kill",wepstring,attacker,victim,-1);

		return Plugin_Continue;
	}
}

// ####### Day of Defeat #######
public Action Event_Hurt(Handle event, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int victim   = GetClientOfUserId(GetEventInt(event, "userid"));
	int headshot   = (GetEventInt(event, "health") == 0 && GetEventInt(event, "hitgroup") == 1);

	if (headshot)
		runSoundEvent(event,"kill","headshot",attacker,victim,-1);

	return Plugin_Continue;
}

// ####### TF2 #######
public Action Event_Build(Handle event, const char[] name, bool dontBroadcast)
{
	char sObject[PLATFORM_MAX_PATH+1];
	int objectint = GetEventInt(event,"object");
	int attacker = GetClientOfUserId(GetEventInt(event, "userid"));
	switch(objectint)
	{
		case 0:
			strcopy(sObject,sizeof(sObject),"obj_dispenser");
		case 1:
			strcopy(sObject,sizeof(sObject),"obj_tele_in");
		case 2:
			strcopy(sObject,sizeof(sObject),"obj_tele_out");
		case 3:
			strcopy(sObject,sizeof(sObject),"obj_sentry");
		default:
			strcopy(sObject,sizeof(sObject),"obj_dispenser");
	}
	runSoundEvent(event,"build",sObject,attacker,0,-1);
	return Plugin_Continue;
}

// Generic Sound event, this gets triggered whenever an event that is supported is triggered
public runSoundEvent(Handle event, const char[] type, const char[] extra, const attacker, int victim, int team)
{
	char action[PLATFORM_MAX_PATH+1];
	char extraparam[PLATFORM_MAX_PATH+1];
	char location[PLATFORM_MAX_PATH+1];
	char playto[PLATFORM_MAX_PATH+1];
	bool result = false;

	if(listfile == null)
		return false;

	KvRewind(listfile);
	if (!KvGotoFirstSubKey(listfile))
		return false;

	// Do while loop that finds out what extra parameter is and plays according sound, also adds random
	do
	{
		KvGetString(listfile, "action",action,sizeof(action),"");
		if (StrEqual(action, type, false))
		{
			if (!KvGetNum(listfile, "enable", 1))
				continue;
			
			// ###### Random Sound ######
			char file[8] = "file";
			int count = KvGetNum(listfile, "count", 1);
			if (count > 1)
				Format(file, sizeof(file), "file%d", GetRandomInt(1,count));

			if (StrEqual(file, "file1"))
				KvGetString(listfile, "file", location, sizeof(location), "");
			else KvGetString(listfile, file, location, sizeof(location),"");

			// ###### Random Sound End ######
			KvGetString(listfile, "param",extraparam,sizeof(extraparam),action);
			if(team == -1)
				KvGetString(listfile, "playto",playto,sizeof(playto),"all");
			else KvGetString(listfile, "playto",playto,sizeof(playto),"RoundEvent");
			
			if(!IsGameSound(location) && !checkSamplingRate(location))
				return false;
			
			if(StrEqual(extra, extraparam, false))
			{
				// Next section performs random calculations, all percents in decimal from 1-0
				float random = KvGetFloat(listfile, "prob",1.0);
				
				// Added error checking  for the random number
				if(random <= 0.0)
				{
					random = 0.01;
					PrintToChatAll("Your random value for (%s) is <= 0, please make it above 0",location);
				}
				else if(random > 1.0)
					random = 1.0;

				float generated = GetRandomFloat(0.0,1.0);
				
				if (generated <= random)
				{
					//### Delay
					float delay = KvGetFloat(listfile, "delay", 0.1);
					if(delay < 0.1)
						delay = 0.1;
					else if (delay > 60.0)
						delay = 60.0;

					Handle pack;
					CreateDataTimer(delay,runSoundEventTimer,pack, TIMER_FLAG_NO_MAPCHANGE);
					WritePackCell(pack, attacker);
					WritePackCell(pack, victim);
					WritePackCell(pack, team);
					WritePackString(pack, playto);
					WritePackString(pack, location);
					ResetPack(pack);
				}
				result = true;
			}
			else result = false;
		}
		else result = false;
	} while (KvGotoNextKey(listfile));
	
	return result;
}

public Action runSoundEventTimer(Handle timer, Handle pack)
{
	int attacker = ReadPackCell(pack);
	int victim = ReadPackCell(pack);
	int iTeam = ReadPackCell(pack);
	char playto[PLATFORM_MAX_PATH+1];
	ReadPackString(pack, playto, sizeof(playto));
	char location[PLATFORM_MAX_PATH+1];
	ReadPackString(pack, location, sizeof(location));

	if (StrEqual(playto, "attacker", false)) // Send to attacker
	{
		if (IsValidClient(attacker) && checkClientCookies(attacker, CHK_EVENTS) && !g_bPlayedEvent2Client[attacker])
		{
			PrepareAndEmitSoundToClient(attacker, location);
			g_bPlayedEvent2Client[attacker] = true;
			CreateTimer(2.0, reset_PlayedEvent2Client, attacker);
		}
	}
	else if (StrEqual(playto, "victim", false)) // Send to victim
	{
		if (IsValidClient(victim)&& checkClientCookies(victim, CHK_EVENTS))
			PrepareAndEmitSoundToClient(victim, location);

	} else if (StrEqual(playto, "both", false)) // Send to attacker & victim
	{
		if (IsValidClient(attacker) && checkClientCookies(attacker, CHK_EVENTS) && !g_bPlayedEvent2Client[attacker])
		{
			PrepareAndEmitSoundToClient(attacker, location);
			g_bPlayedEvent2Client[attacker] = true;
			CreateTimer(3.0, reset_PlayedEvent2Client, attacker);
		}
		if (IsValidClient(victim)&& checkClientCookies(victim, CHK_EVENTS))
		{
			PrepareAndEmitSoundToClient(victim, location);
		}

	}
	else if (StrEqual(playto, "ateam", false)) // Send to attacker team
	{
		int aTeam = GetClientTeam(attacker);
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i) && GetClientTeam(i) == aTeam && checkClientCookies(i, CHK_EVENTS))
				PrepareAndEmitSoundToClient(i, location);
		}

	} 
	else if (StrEqual(playto, "vteam", false)) // Send to victim team
	{ 
		int vTeam = GetClientTeam(victim);
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i) && GetClientTeam(i) == vTeam && checkClientCookies(i, CHK_EVENTS))
				PrepareAndEmitSoundToClient(i, location);
		}
	} 
	else if (StrEqual(playto, "RoundEvent", false))
	{ 
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i))
			{
				if(GetClientTeam(i) == iTeam)
					PrepareAndEmitSoundToClient(i, location);
			}
		}
	}
	else // Send to all clients
	{
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i) && checkClientCookies(i, CHK_EVENTS))
				PrepareAndEmitSoundToClient(i, location);
		}
	}
}

public OnClientPostAdminCheck(client)
{
	char auth[64];
	if (GetClientAuthId(client, AuthId_SteamID64, auth, sizeof(auth), true))
	{
		if(IsValidClient(client) && !GetConVarBool(cvarjoinspawn))
			CheckJoin(client, auth);

		if (g_hSoundCountTrie == null || !GetTrieValue(g_hSoundCountTrie, auth, SndCount[client]))
			SndCount[client] = 0;
	}
	else SndCount[client] = 0;
}

public void PlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	if(!GetConVarBool(cvarjoinspawn) || !GetConVarBool(cvarjoinexit))
		return;
	
	int userid = GetEventInt(event,"userid");
	if(!userid)
		return;
	
	int client = GetClientOfUserId(userid);
	if (!client)
		return;
	
	if (IsFakeClient(client))
		return;
		
	if (!firstSpawn[client])
		return;
		
	char auth[64];
	if(GetClientAuthId(client, AuthId_SteamID64, auth, sizeof(auth), true))
	{
		CheckJoin(client, auth);
		firstSpawn[client] = false;
	}
}

public void CheckJoin(client, const char[] auth)
{
	if(GetConVarBool(cvarspecificjoinexit))
	{
		char filelocation[PLATFORM_MAX_PATH+1];
		KvRewind(listfile);
		if (KvJumpToKey(listfile, auth) && !checkClientCookies(client, CHK_GREETED))
		{
			filelocation[0] = '\0';
			KvGetString(listfile, "join", filelocation, sizeof(filelocation), "");
			if (filelocation[0] != '\0')
			{
				Send_Sound(client,filelocation, "", true, false);
				SetClientCookie(client, g_ssgreeted_cookie, "1");
				return;
			}
			else if (Submit_Sound(client, "", true, false))
			{
				SetClientCookie(client, g_ssgreeted_cookie, "1");
				return;
			}
		}
	}

	if(GetConVarBool(cvarjoinexit) || GetConVarBool(cvarjoinspawn))
	{
		KvRewind(listfile);
		if (KvJumpToKey(listfile, "JoinSound") && !checkClientCookies(client, CHK_GREETED))
		{
			Submit_Sound(client,"", true, false);
			SetClientCookie(client, g_ssgreeted_cookie, "1");
		}
	}
}

public void OnClientDisconnect(int client)
{
	if(GetConVarBool(cvarjoinexit) && listfile != null)
	{
		firstSpawn[client] = true;

		if(GetConVarBool(cvarspecificjoinexit))
		{
			char auth[64];
			if(GetClientAuthId(client, AuthId_SteamID64, auth, sizeof(auth), true))
			{
				char filelocation[PLATFORM_MAX_PATH+1];
				KvRewind(listfile);
				if (KvJumpToKey(listfile, auth))
				{
					filelocation[0] = '\0';
					KvGetString(listfile, "exit", filelocation, sizeof(filelocation), "");
					if (filelocation[0] != '\0')
					{
						Send_Sound(client,filelocation, "", false, true);
						return;
					}
					else if (Submit_Sound(client,"", false, true))
						return;
				}
			}
		}

		KvRewind(listfile);
		if (KvJumpToKey(listfile, "ExitSound"))
			Submit_Sound(client,"", false, true);
	}
}

public void OnClientAuthorized(int client, const char[] auth)
{
	if(client != 0)
		firstSpawn[client] = true;
}

public Action Command_Say(client, const char[] command, int argc)
{
	if(IsValidClient(client))
	{
		// If sounds are not enabled, then skip this whole thing
		if (!GetConVarBool(cvarsoundenable))
			return Plugin_Continue;

		// player is banned from playing sounds
		if (checkClientCookies(client, CHK_BANNED))
			return Plugin_Continue;
			
		char speech[192];
		char stopFlags[26];
		stopFlags[0] = '\0';
		char confMenuFlags[26];
		confMenuFlags[0] = '\0';
		
		GetConVarString(cvarStopFlags, stopFlags, sizeof(stopFlags));
		GetConVarString(cvarMenuSettingsFlags, confMenuFlags, sizeof(confMenuFlags));
		
		if (GetCmdArgString(speech, sizeof(speech)) < 1)
			return Plugin_Continue;
		
		int startidx = 0;
		
		if (speech[strlen(speech)-1] == '"')
		{
			speech[strlen(speech)-1] = '\0';
			startidx = 1;
		}

		if (strcmp(command, "say2", false) == 0)
			startidx += 4;

		if(strcmp(speech[startidx],"!sounds",false) == 0 || strcmp(speech[startidx],"sounds",false) == 0)
		{
			if (confMenuFlags[0] == '\0' || HasClientFlags(confMenuFlags, client))
				ShowClientPrefMenu(client);

			return Plugin_Handled;

		} 
		else if(strcmp(speech[startidx],"!soundlist",false) == 0 || strcmp(speech[startidx],"soundlist",false) == 0)
		{
			if(GetConVarInt(cvarshowsoundmenu) == 1)
				Sound_Menu(client,normal_sounds);
			else 
			{
				List_Sounds(client);
				PrintToChat(client,"\x04[Say Sounds]\x01%t", "Soundlist");
			}
			return Plugin_Handled;

		} 
		else if(strcmp(speech[startidx],"!soundmenu",false) == 0 || strcmp(speech[startidx],"soundmenu",false) == 0)
		{
			if(GetConVarInt(cvarshowsoundmenu) == 1)
				Sound_Menu(client,normal_sounds);
			
			return Plugin_Handled;
		} 
		else if(strcmp(speech[startidx],"!adminsounds",false) == 0 || strcmp(speech[startidx],"adminsounds",false) == 0)
		{
			Sound_Menu(client,admin_sounds);
			return Plugin_Handled;

		} 
		else if(strcmp(speech[startidx],"!karaoke",false) == 0 || strcmp(speech[startidx],"karaoke",false) == 0)
		{
			Sound_Menu(client,karaoke_sounds);
			return Plugin_Handled;

		} 
		else if(strcmp(speech[startidx],"!allsounds",false) == 0 || strcmp(speech[startidx],"allsounds",false) == 0)
		{
			Sound_Menu(client,all_sounds);
			return Plugin_Handled;

		} 
		else if(strcmp(speech[startidx],"!stop",false) == 0)
		{
			if(SndPlaying[client][0] && (stopFlags[0] == '\0' || HasClientFlags(stopFlags, client)))
			{
				StopSound(client,SNDCHAN_AUTO,SndPlaying[client]);
				SndPlaying[client] = "";
			}
			return Plugin_Handled;
		}

		// If player has turned sounds off and is restricted from playing sounds, skip
		if(!GetConVarBool(cvarplayifclsndoff) && !checkClientCookies(client, CHK_SAYSOUNDS)) 
			return Plugin_Continue;

		KvRewind(listfile);
		KvGotoFirstSubKey(listfile);
		bool sentence = GetConVarBool(cvarsentence);
		bool adult;
		bool trigfound;
		char buffer[255];
		
		do{
			KvGetSectionName(listfile, buffer, sizeof(buffer));
			adult = view_as<bool>(KvGetNum(listfile, "adult",0));
			if ((sentence && StrContains(speech[startidx], buffer, false) >= 0) || 
				(strcmp(speech[startidx], buffer, false) == 0))
				{

					Submit_Sound(client,buffer);
					trigfound = true;
					break;
			}
		} while (KvGotoNextKey(listfile));

		if(GetConVarBool(cvarblocktrigger) && trigfound && !StrEqual(LastPlayedSound, buffer, false))
			return Plugin_Handled;
		else if(adult && trigfound)
			return Plugin_Handled;
		else return Plugin_Continue;
	}	
	
	return Plugin_Continue;
}

bool Submit_Sound(int client, const char[] name, bool joinsound = false, bool exitsound = false)
{
	if (KvGetNum(listfile, "enable", 1))
	{
		char filelocation[PLATFORM_MAX_PATH+1];
		char file[8] = "file";
		int count = KvGetNum(listfile, "count", 1);
		if (count > 1)
			Format(file, sizeof(file), "file%d", GetRandomInt(1, count));

		filelocation[0] = '\0';
		KvGetString(listfile, file, filelocation, sizeof(filelocation));
		if (filelocation[0] == '\0' && StrEqual(file, "file1"))
			KvGetString(listfile, "file", filelocation, sizeof(filelocation), "");

		if (filelocation[0] != '\0')
		{
			char karaoke[PLATFORM_MAX_PATH+1];
			karaoke[0] = '\0';
			KvGetString(listfile, "karaoke", karaoke, sizeof(karaoke));
			if (karaoke[0] != '\0')
				Load_Karaoke(client, filelocation, name, karaoke);
			else Send_Sound(client, filelocation, name, joinsound, exitsound);

			return true;
		}
	}
	return false;
}

void Send_Sound(client, const char[] filelocation, const char[] name, bool joinsound = false, bool exitsound = false)
{
	int tmp_joinsound;

	int adminonly = KvGetNum(listfile, "admin",0);
	int adultonly = KvGetNum(listfile, "adult",0);
	int singleonly = KvGetNum(listfile, "single",0);

	char txtmsg[256];
	txtmsg[0] = '\0';

	if (joinsound)
		KvGetString(listfile, "text", txtmsg, sizeof(txtmsg));
	if (exitsound)
		KvGetString(listfile, "etext", txtmsg, sizeof(txtmsg));
	if (!joinsound && !exitsound)
		KvGetString(listfile, "text", txtmsg, sizeof(txtmsg));

	int actiononly = KvGetNum(listfile, "actiononly",0);
	
	char accflags[26];
	accflags[0] = '\0';
	
	KvGetString(listfile, "flags", accflags, sizeof(accflags));

	if (joinsound || exitsound)
		tmp_joinsound = 1;
	else tmp_joinsound = 0;
	
	int timebuf;
	int samplerate;
	
	char sFullPath[PLATFORM_MAX_PATH];
	FormatEx(sFullPath, PLATFORM_MAX_PATH, "sound/%s", filelocation);
	
	if(FileExists(sFullPath, false))
	{
		Handle h_Soundfile = null;
		h_Soundfile = OpenSoundFile(filelocation, true);

		if(h_Soundfile != null)
		{
			timebuf = GetSoundLength(h_Soundfile);
			samplerate = GetSoundSamplingRate(h_Soundfile);
			delete h_Soundfile;
		}
		else LogError("<Send_Sound> null for file \"%s\" ", filelocation);

		// Check the sample rate and leave a message if it's above 44.1 kHz;
		if (samplerate > 44100)
		{
			LogError("Invalid sample rate (\%d Hz) for file \"%s\", sample rate should not be above 44100 Hz", samplerate, filelocation);
			PrintToChat(client, "\x04[Say Sounds] \x01Invalid sample rate (\x04%d Hz\x01) for file \x04%s\x01, sample rate should not be above \x0444100 Hz", samplerate, filelocation);
			return;
		}
	}

	float duration = float(timebuf);

	float defVol = GetConVarFloat(cvarvolume);
	float volume = KvGetFloat(listfile, "volume", defVol);
	if (volume == 0.0 || volume == 1.0)
		volume = defVol; // do this check because of possibly "stupid" values in cfg file

	// ### Delay ###
	float delay = KvGetFloat(listfile, "delay", 0.1);
	if(delay < 0.1)
		delay = 0.1;
	else if (delay > 60.0)
		delay = 60.0;

	Handle pack;
	CreateDataTimer(delay, Play_Sound_Timer, pack, TIMER_FLAG_NO_MAPCHANGE);
	WritePackCell(pack, client);
	WritePackCell(pack, adminonly);
	WritePackCell(pack, adultonly);
	WritePackCell(pack, singleonly);
	WritePackCell(pack, actiononly);
	WritePackFloat(pack, duration);
	WritePackFloat(pack, volume);
	WritePackString(pack, filelocation);
	WritePackString(pack, name);
	WritePackCell(pack, tmp_joinsound);
	WritePackString(pack, txtmsg);
	WritePackString(pack, accflags);
	ResetPack(pack);
}

void Play_Sound(const char[] filelocation, float volume)
{
	int clientlist[MAXPLAYERS+1];
	int clientcount = 0;
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsValidClient(i) && checkClientCookies(i, CHK_SAYSOUNDS) && HearSound(i))
		{
			clientlist[clientcount++] = i;
			if (GetConVarBool(cvarinterruptsound))
				StopSound(i, SNDCHAN_AUTO, SndPlaying[i]);
			strcopy(SndPlaying[i], sizeof(SndPlaying[]), filelocation);
		}
	}
	if (clientcount)
		PrepareAndEmitSound(clientlist, clientcount, filelocation, .volume=volume);
}

public Action Play_Sound_Timer(Handle timer, Handle pack)
{
	char filelocation[PLATFORM_MAX_PATH+1];
	char name[PLATFORM_MAX_PATH+1];
	char chatBuffer[256];
	char txtmsg[256];
	txtmsg[0] = '\0';
	char accflags[26];
	accflags[0] = '\0';
	int client = ReadPackCell(pack);
	int adminonly = ReadPackCell(pack);
	int adultonly = ReadPackCell(pack);
	int singleonly = ReadPackCell(pack);
	int actiononly = ReadPackCell(pack);
	float duration = ReadPackFloat(pack);
	float volume = ReadPackFloat(pack);
	ReadPackString(pack, filelocation, sizeof(filelocation));
	ReadPackString(pack, name , sizeof(name));
	int joinsound = ReadPackCell(pack);
	ReadPackString(pack, txtmsg , sizeof(txtmsg));
	ReadPackString(pack, accflags , sizeof(accflags));
	
	if (IsValidClient(client))
	{
		if(actiononly == 1)
		{
			PrintToChat(client,"\x04[Action Sounds] \x01%t", "ActionSounds");
			return Plugin_Handled;
		}
	}

	float waitTime = GetConVarFloat(cvartimebetween);
	float adminTime = GetConVarFloat(cvaradmintime);
	char waitTimeFlags[26];
	waitTimeFlags[0] = '\0';
	GetConVarString(cvartimebetweenFlags, waitTimeFlags, sizeof(waitTimeFlags));

	bool isadmin = false;
	if (IsValidClient(client))
	{
		AdminId aid = GetUserAdmin(client);
		isadmin = (aid != INVALID_ADMIN_ID) && GetAdminFlag(aid, Admin_Generic, Access_Effective);
		if(adminonly && !isadmin)
		{
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "AdminSounds");
			return Plugin_Handled;
		}
		if (accflags[0] != '\0' && !HasClientFlags(accflags, client))
		{
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "NoAccess");
			return Plugin_Handled;
		}
	}

	float thetime = GetGameTime();
	
	if (globalLastSound > 0.0)
	{
		if (waitTimeFlags[0] == '\0' || !HasClientFlags(waitTimeFlags, client))
		{
			if ((!isadmin && globalLastSound > thetime) || (isadmin && adminTime >= 0.0 && globalLastSound > thetime))
			{
				if(IsValidClient(client))
				{
					int cooldown = RoundToFloor(1 + globalLastSound - thetime);
					PrintToChat(client,"\x04[Say Sounds] \x01%t", "SpamSounds", cooldown);
				}
				return Plugin_Handled;
			}
		}
	}
	
	if (waitTime > 0.0 && waitTime < duration)
		waitTime = duration;
	
	if (adminonly)
	{
		if (globalLastAdminSound >= thetime)
		{
			if(IsValidClient(client))
			{
				int cooldown = RoundToFloor(1 + globalLastAdminSound - thetime);
				PrintToChat(client,"\x04[Say Sounds] \x01%t", "SpamAdminSounds", cooldown);
			}
			return Plugin_Handled;
		}

		if(adminTime > 0.0 && adminTime < duration)
			adminTime = duration;
	}

	if(GetConVarBool(cvarexcludelastsound) && IsValidClient(client) && joinsound != 1 && StrEqual(LastPlayedSound, name, false))
	{
		PrintToChat(client, "\x04[Say Sounds] \x01%t", "RecentlyPlayed");
		return Plugin_Handled;
	}

	if(GetConVarBool(cvarfilterifdead))
	{
		if(IsDeadClient(client))
			hearalive = false;
		else hearalive = true;
	}
	
	char soundFlags[26];
	soundFlags[0] = '\0';
	GetConVarString(cvarsoundFlags, soundFlags, sizeof(soundFlags));
	
	int soundLimit;
	
	if (isadmin)
		soundLimit = GetConVarInt(cvaradminlimit);
	else if (soundFlags[0] != '\0' && HasClientFlags(soundFlags, client))
		soundLimit = GetConVarInt(cvarsoundFlagsLimit);
	else soundLimit = GetConVarInt(cvarsoundlimit);
	
	char soundLimitFlags[26];
	soundLimitFlags[0] = '\0';
	GetConVarString(cvarsoundlimitFlags, soundLimitFlags, sizeof(soundLimitFlags));
	
	if (soundLimit <= 0 || SndCount[client] < soundLimit)
	{
		if (joinsound == 1)
		{
			if (txtmsg[0] != '\0')
				dispatchChatMessage(client, txtmsg, "");
		}
		else
		{
			if (soundLimitFlags[0] == '\0' || !HasClientFlags(soundLimitFlags, client))
				SndCount[client]++;
			
			globalLastSound   = thetime + waitTime;
		}
		if (adminonly)
			globalLastAdminSound = thetime + adminTime;

		if (singleonly && joinsound == 1)
		{
			if(checkClientCookies(client, CHK_SAYSOUNDS) && IsValidClient(client))
			{
				PrepareAndEmitSoundToClient(client, filelocation, .volume=volume);
				strcopy(SndPlaying[client], sizeof(SndPlaying[]), filelocation);
				if (GetConVarBool(cvarlogging))
				{
					LogToGame("[Say Sounds Log] %s%N  played  %s%s(%s)", isadmin ? "Admin " : "", client,
							adminonly ? "admin sound " : "", name, filelocation);
				}
			}
		}
		else
		{
			Play_Sound(filelocation, volume);
			PlayCounter(name);
			LastPlayedSound = name;
			if (name[0] && IsValidClient(client))
			{
				if (GetConVarBool(cvarannounce))
				{
					if(adultonly && GetConVarBool(cvaradult))
					{
						if (txtmsg[0] != '\0')
						{
							Format(chatBuffer, sizeof(chatBuffer), "\x04%N\x01: %s", client , txtmsg);
							dispatchChatMessage(client, chatBuffer, "");
						}
						else dispatchChatMessage(client, "PlayedAdultSound", "", true);
					}
					else
					{
						if (txtmsg[0] != '\0')
						{
							Format(chatBuffer, sizeof(chatBuffer), "\x04%N\x01: %s", client , txtmsg);
							dispatchChatMessage(client, chatBuffer, "");
						}
						else dispatchChatMessage(client, "PlayedSound", name, true);
					}
				}
				if (GetConVarBool(cvarlogging))
				{
					LogToGame("[Say Sounds Log] %s%N  played  %s%s(%s)", isadmin ? "Admin " : "", client,
							adminonly ? "admin sound " : "", name, filelocation);
				}
			}
			else if (GetConVarBool(cvarlogging))
				LogToGame("[Say Sounds Log] played %s", filelocation);
		}
	}

	if(soundLimit > 0 && IsValidClient(client))
	{
		if (SndCount[client] > soundLimit)
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "QuotaReched");
		else if (SndCount[client] == soundLimit && joinsound != 1)
		{
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "NoSoundsLeft");
			SndCount[client]++; // Increment so we get the sorry message next time.
		}
		else
		{
			int soundWarn = isadmin ? GetConVarInt(cvaradminwarn) : GetConVarInt(cvarsoundwarn);	
			if (soundWarn <= 0 || SndCount[client] >= soundWarn)
			{
				if (joinsound != 1 && (soundLimitFlags[0] == '\0' || !HasClientFlags(soundLimitFlags, client)))
				{
					int numberleft = (soundLimit -  SndCount[client]);
					if (numberleft == 1)
						PrintToChat(client,"\x04[Say Sounds] \x01%t", "SoundLeft",numberleft);
					else PrintToChat(client,"\x04[Say Sounds] \x01%t", "SoundLeftPlural",numberleft);
				}
			}
		}
	}
	return Plugin_Handled;
}

void dispatchChatMessage(int client, const char[] message, const char[] name, bool translate = false)
{
	for(int i = 1; i <= MaxClients; i++) 
	{
		if(IsValidClient(i) && checkClientCookies(i, CHK_CHATMSG))
		{
			if(translate && StrEqual(name, ""))
				PrintToChat(i, "%t", message);
			else if(translate && !StrEqual(name, ""))
				PrintToChat(i, "%t", message, client, name);
			else
				PrintToChat(i, message);
		}
	}
}

public void Load_Karaoke(int client, const char[] filelocation, const char[] name, const char[] karaoke)
{
	int adminonly = KvGetNum(listfile, "admin", 1); // Karaoke sounds default to admin only
	bool isadmin = false;
	if (IsValidClient(client))
	{
		AdminId aid = GetUserAdmin(client);
		isadmin = (aid != INVALID_ADMIN_ID) && GetAdminFlag(aid, Admin_Generic, Access_Effective);
		if(adminonly && !isadmin)
		{
			PrintToChat(client,"\x04[Say Sounds] \x01%t", "AdminSounds");
			return;
		}
	}

	char karaokecfg[PLATFORM_MAX_PATH+1];
	BuildPath(Path_SM,karaokecfg,sizeof(karaokecfg),"configs/%s",karaoke);
	if(!FileExists(karaokecfg))
	{
		LogError("%s not parsed...file doesnt exist!", karaokecfg);
		Send_Sound(client, filelocation, name);
	}
	else
	{
		if (karaokeFile != null)
			delete karaokeFile;
		
		karaokeFile = CreateKeyValues(name);
		FileToKeyValues(karaokeFile,karaokecfg);
		KvRewind(karaokeFile);
		char title[128];
		title[0] = '\0';
		KvGetSectionName(karaokeFile, title, sizeof(title));
		if (KvGotoFirstSubKey(karaokeFile))
		{
			float time = GetConVarFloat(cvarkaraokedelay);
			if (time > 0.0)
				Karaoke_Countdown(client, filelocation, title[0] ? title : name, time, true);
			else
				Karaoke_Start(client, filelocation, name);
		}
		else
		{
			LogError("%s not parsed...No subkeys found!", karaokecfg);
			Send_Sound(client, filelocation, name);
		}
	}
}

void Karaoke_Countdown(int client, const char[] filelocation, const char[] name, float time, bool attention)
{
	float next=0.0;

	char announcement[128];
	if (attention)
	{
		Show_Message("%s\nKaraoke will begin in %2.0f seconds", name, time);
		if (GetGameType() == tf2)
			strcopy(announcement, sizeof(announcement), TF2_ATTENTION);
		else
			strcopy(announcement, sizeof(announcement), HL2_ATTENTION);

		if (time >= 20.0)
			next = 20.0;
		else if (time >= 10.0)
			next = 10.0;
		else if (time > 5.0)
			next = 5.0;
		else next = time - 1.0;
	}
	else
	{
		if (GetGameType() == tf2)
			Format(announcement, sizeof(announcement), "vo/announcer_begins_%dsec.wav", RoundToFloor(time));
		else
		{
			if (time == 10.0)
				strcopy(announcement, sizeof(announcement), HL2_10_SECONDS);
			else if (time == 5.0)
				strcopy(announcement, sizeof(announcement), HL2_5_SECONDS);
			else if (time == 4.0)
				strcopy(announcement, sizeof(announcement), HL2_4_SECONDS);
			else if (time == 3.0)
				strcopy(announcement, sizeof(announcement), HL2_3_SECONDS);
			else if (time == 2.0)
				strcopy(announcement, sizeof(announcement), HL2_2_SECONDS);
			else if (time == 1.0)
				strcopy(announcement, sizeof(announcement), HL2_1_SECOND);
			else announcement[0] = '\0';
		}
		switch (time)
		{
			case 20.0: next = 10.0;
			case 10.0: next = 5.0;
			case 5.0:  next = 4.0;
			case 4.0:  next = 3.0;
			case 3.0:  next = 2.0; 
			case 2.0:  next = 1.0; 
			case 1.0:  next = 0.0; 
		}
	}

	if (time > 0.0)
	{
		if (announcement[0] != '\0')
		{
			Play_Sound(announcement, 1.0);
			PlayCounter(name);
		}

		Handle pack;
		karaokeTimer = CreateDataTimer(time - next, Karaoke_Countdown_Timer, pack, TIMER_FLAG_NO_MAPCHANGE);
		WritePackCell(pack, client);
		WritePackFloat(pack, next);
		WritePackString(pack, filelocation);
		WritePackString(pack, name);
		ResetPack(pack);
	}
	else Karaoke_Start(client, filelocation, name);
}

void Karaoke_Start(int client, const char[] filelocation, const char[] name)
{
	char text[3][128], timebuf[64];
	timebuf[0] = '\0';
	text[0][0] = '\0';
	text[1][0] = '\0';
	text[2][0] = '\0';

	KvGetString(karaokeFile,"text",text[0],sizeof(text[]));
	KvGetString(karaokeFile,"text1",text[1],sizeof(text[]));
	KvGetString(karaokeFile,"text2",text[2],sizeof(text[]));
	KvGetString(karaokeFile,"time",timebuf,sizeof(timebuf));

	float time = Convert_Time(timebuf);
	if (time == 0.0)
	{
		Karaoke_Message(text);
		if (KvGotoNextKey(karaokeFile))
		{
			text[0][0] = '\0';
			text[1][0] = '\0';
			text[2][0] = '\0';
			KvGetString(karaokeFile,"text",text[0],sizeof(text[]));
			KvGetString(karaokeFile,"text1",text[1],sizeof(text[]));
			KvGetString(karaokeFile,"text2",text[2],sizeof(text[]));
			KvGetString(karaokeFile,"time",timebuf,sizeof(timebuf));
			time = Convert_Time(timebuf);
		}
		else
		{
			delete karaokeFile;
			karaokeFile = null;
			time = 0.0;
		}
	}

	if (time > 0.0)
	{
		cvaradvertisements = FindConVar("sm_advertisements_enabled");
		if (cvaradvertisements != null)
		{
			advertisements_enabled = GetConVarBool(cvaradvertisements);
			SetConVarBool(cvaradvertisements, false);
		}

		Handle pack;
		karaokeTimer = CreateDataTimer(time, Karaoke_Timer, pack, TIMER_FLAG_NO_MAPCHANGE);
		WritePackString(pack, text[0]);
		WritePackString(pack, text[1]);
		WritePackString(pack, text[2]);
		ResetPack(pack);
	}
	else karaokeTimer = null;

	karaokeStartTime = GetEngineTime();
	Send_Sound(client, filelocation, name, true);
}

float Convert_Time(const char[] buffer)
{
	char part[5];
	int pos = SplitString(buffer, ":", part, sizeof(part));
	if (pos == -1)
		return StringToFloat(buffer);
	else
	{
		// Convert from mm:ss to seconds
		return (StringToFloat(part) * 60.0) + StringToFloat(buffer[pos]);
	}
}

void Karaoke_Message(const char[][] text)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsValidClient(i) && checkClientCookies(i, CHK_KARAOKE))
		{
			int team = GetClientTeam(i) - 1;
			if (team >= 1 && text[team][0] != '\0')
				PrintCenterText(i, text[team]);
			else PrintCenterText(i, text[0]);
		}
	}
}

void Show_Message(const char[] fmt, any...)
{
	char text[128];
	VFormat(text, sizeof(text), fmt, 2);
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(IsValidClient(i) && checkClientCookies(i, CHK_KARAOKE))
			PrintCenterText(i, text);
	}
}

public Action Karaoke_Countdown_Timer(Handle timer, Handle pack)
{
	char filelocation[PLATFORM_MAX_PATH+1];
	char name[PLATFORM_MAX_PATH+1];
	int client = ReadPackCell(pack);
	float time = ReadPackFloat(pack);
	ReadPackString(pack, filelocation , sizeof(filelocation));
	ReadPackString(pack, name , sizeof(name));
	Karaoke_Countdown(client, filelocation, name, time, false);
}

public Action Karaoke_Timer(Handle timer, Handle pack)
{
	char text[3][128], timebuf[64];
	timebuf[0] = '\0';
	text[0][0] = '\0';
	text[1][0] = '\0';
	text[2][0] = '\0';

	ReadPackString(pack, text[0], sizeof(text[]));
	ReadPackString(pack, text[1], sizeof(text[]));
	ReadPackString(pack, text[2], sizeof(text[]));
	Karaoke_Message(text);

	if (karaokeFile != null)
	{
		if (KvGotoNextKey(karaokeFile))
		{
			text[0][0] = '\0';
			text[1][0] = '\0';
			text[2][0] = '\0';
			KvGetString(karaokeFile,"text",text[0],sizeof(text[]));
			KvGetString(karaokeFile,"text1",text[1],sizeof(text[]));
			KvGetString(karaokeFile,"text2",text[2],sizeof(text[]));
			KvGetString(karaokeFile,"time",timebuf,sizeof(timebuf));
			float time = Convert_Time(timebuf);
			float current_time = GetEngineTime() - karaokeStartTime;

			Handle next_pack;
			karaokeTimer = CreateDataTimer(time-current_time, Karaoke_Timer, next_pack, TIMER_FLAG_NO_MAPCHANGE);
			WritePackString(next_pack, text[0]);
			WritePackString(next_pack, text[1]);
			WritePackString(next_pack, text[2]);
			ResetPack(next_pack);
		}
		else
		{
			delete karaokeFile;
			karaokeFile = null;
			karaokeTimer = null;
			karaokeStartTime = 0.0;

			if (cvaradvertisements != null)
			{
				SetConVarBool(cvaradvertisements, advertisements_enabled);
				delete cvaradvertisements;
				cvaradvertisements = null;
			}
		}
	}
	else karaokeTimer = null;
}

public Action Command_Sound_Reset(int client, int args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "QuotaResetUsage");
		return Plugin_Handled;
	}

	char arg[64];
	GetCmdArg(1, arg, sizeof(arg));	

	if (strcmp(arg,"all",false) == 0 )
	{
		for (int i = 1; i <= MAXPLAYERS; i++)
			SndCount[i] = 0;

		if(client !=0)
			ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "QuotaResetAll");
	}
	else
	{
		char name[64];
		bool isml,clients[MAXPLAYERS+1];
		int count = ProcessTargetString(arg, client, clients, MAXPLAYERS + 1, COMMAND_FILTER_NO_BOTS, name, sizeof(name), isml);
		if (count > 0)
		{
			for (new x = 0; x < count; x++)
			{
				int player=clients[x];
				if(IsPlayerAlive(player))
				{
					SndCount[player] = 0;
					char clientname[64];
					GetClientName(player,clientname,MAXPLAYERS);
					ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "QuotaResetUser", clientname);
				}
			}
		}
		else ReplyToTargetError(client, count);
	}
	return Plugin_Handled;
}

public Action Command_Sound_Ban(int client, int args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "SoundBanUsage");
		return Plugin_Handled;	
	}

	char arg[64];
	GetCmdArg(1, arg, sizeof(arg));	

	char name[64];
	bool isml,clients[MAXPLAYERS+1];
	int count = ProcessTargetString(arg, client, clients, MAXPLAYERS + 1, COMMAND_FILTER_NO_BOTS, name, sizeof(name), isml);
	if (count > 0)
	{
		for (int x = 0; x < count; x++)
		{
			int player=clients[x];
			if(IsClientConnected(player) && IsClientInGame(player))
			{
				char clientname[64];
				GetClientName(player,clientname,MAXPLAYERS);
				if (checkClientCookies(player, CHK_BANNED))
					ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "AlreadyBanned", clientname);
				else
				{
					SetClientCookie(player, g_ssban_cookie, "on");
					ReplyToCommand(client,"\x04[Say Sounds] \x01%t", "PlayerBanned", clientname);
				}
			}
		}
	}
	else ReplyToTargetError(client, count);

	return Plugin_Handled;
}

public Action Command_Sound_Unban(int client, int args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "\x04[Say Sounds] \x01%t", "SoundUnbanUsage");
		return Plugin_Handled;	
	}

	char arg[64];
	GetCmdArg(1, arg, sizeof(arg));	

	char name[64];
	bool isml,clients[MAXPLAYERS+1];
	int count = ProcessTargetString(arg, client, clients, MAXPLAYERS + 1, COMMAND_FILTER_NO_BOTS, name, sizeof(name), isml);
	if (count > 0)
	{
		for(int x=0;x<count;x++)
		{
			int player=clients[x];
			if(IsClientConnected(player) && IsClientInGame(player))
			{
				char clientname[64];
				GetClientName(player,clientname,MAXPLAYERS);
				if(!checkClientCookies(player, CHK_BANNED))
				{
					ReplyToCommand(client,"\x04[Say Sounds] \x01%t", "NotBanned", clientname);
				}
				else
				{
					SetClientCookie(player, g_ssban_cookie, "off");
					ReplyToCommand(client,"\x04[Say Sounds] \x01%t", "PlayerUnbanned", clientname);
				}
			}
		}
	}
	else ReplyToTargetError(client, count);

	return Plugin_Handled;
}

public Action Command_Sound_List(int client, int args)
{
	List_Sounds(client);
}

void List_Sounds(int client)
{
	KvRewind(listfile);
	if (KvJumpToKey(listfile, "ExitSound", false)) // Skip exit sound
		KvGotoNextKey(listfile, true);
	else KvGotoFirstSubKey(listfile);

	char buffer[PLATFORM_MAX_PATH+1];
	do
	{
		KvGetSectionName(listfile, buffer, sizeof(buffer));
		PrintToConsole(client, buffer);
	} while (KvGotoNextKey(listfile));
}

public Action Command_Sound_Menu(int client, int args)
{
	Sound_Menu(client, normal_sounds);
}

public Action Command_Admin_Sounds(int client, int args)
{
	Sound_Menu(client, admin_sounds);
}

public Action Command_Karaoke(int client, int args)
{
	Sound_Menu(client, karaoke_sounds);
}

public Action Command_All_Sounds(int client, int args)
{
	Sound_Menu(client, all_sounds);
}

void ClearSoundCountTrie()
{
	// if there are no limits, there is no need to save counts
	if (GetConVarBool(cvarTrackDisconnects) &&
		(GetConVarInt(cvarsoundlimit) > 0 ||
		 GetConVarInt(cvaradminlimit) > 0))
	{
		if (g_hSoundCountTrie == null)
			g_hSoundCountTrie = CreateTrie();
		else ClearTrie(g_hSoundCountTrie);
	}
	else if (g_hSoundCountTrie)
		delete g_hSoundCountTrie;
}